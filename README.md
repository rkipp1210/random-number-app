# **Ross Kippenbrock Random Number App** #

This is a random number factory app

## Sources used for assistance ##
### Authentication ###
* http://www.sitepoint.com/local-authentication-using-passport-node-js/
* https://code.tutsplus.com/tutorials/authenticating-nodejs-applications-with-passport--cms-21619
* http://scotch.io/tutorials/javascript/upgrading-our-easy-node-authentication-series-to-expressjs-4-0
* passport docs
* http://codetheory.in/using-the-node-js-bcrypt-module-to-hash-and-safely-store-passwords/

### jstree ###
* Documentation

### context.js ###
* Documentation

### express ###
* Documentation

### socket.io ###
*  Chat example -> http://socket.io/demos/chat/
* Documentation

### MongoDB ###
* Documentation

### Mongoose ###
* Documentation